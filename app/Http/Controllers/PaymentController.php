<?php

namespace App\Http\Controllers;

use App\Http\Services\KBBService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class PaymentController extends Controller
{


    private $mailing_list = [];
    private $ipForValidation = "";

    public function __construct()
    {
        $this->ipForValidation = env('BANK_REMOTE_IP');
    }

    public function confirmPayment(Request $request) {
        if (request()->server('REMOTE_ADDR') != $this->ipForValidation) {
            abort(500);
        }
        $this->mailing_list = [env('EMAIL_FOR_ALL'), env('EMAIL_FOR_SUCCESS')];
        Mail::raw("Payment Success",function($message) {
            $message->to($this->mailing_list);
        });
    }

    public function paymentError(Request $request) {
        if (request()->server('REMOTE_ADDR') != $this->ipForValidation) {
            abort(500);
        }
        $this->mailing_list = [env('EMAIL_FOR_ALL')];
        Mail::raw("Payment failure",function($message) {
            $message->to($this->mailing_list);
        });
    }

    public function showForm() {
        $sign = $this->generatePayment();
        return view('welcome', compact('sign'));
    }

    private function generatePayment()
    {
        $service = new KBBService();
        $xml = $service->generateSign();
        return base64_encode($xml);
    }


}
