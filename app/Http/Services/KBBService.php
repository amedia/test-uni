<?php

namespace App\Http\Services;

class KBBService
{

    private $invert = 1;
    private $privateKey = "";

    private $merchantCertId = "";
    private $merchantName = "";
    private $orderId = 0;
    private $amount = 0;
    private $currency = 0;
    private $merchantID = "";

    public function __construct()
    {
        $this->orderId = mt_rand(100000, 999999);
        $this->merchantCertId = env('MERCHANT_CERT_ID');
        $this->merchantName = env('MERCHANT_NAME');
        $this->amount = env('AMOUNT');
        $this->currency = env('CURRENCY');
        $this->merchantID = env('MERCHANT_ID');
        $this->loadPrivateKey(env('PRIVATE_KEY_FILE'), env('PRIVATE_KEY_PASSWORD'));

    }


    public function generateSign() {
        $merchant = '<merchant cert_id="' . $this->merchantCertId. '" name="' . $this->merchantName . '"><order order_id="' . $this->orderId . '" amount="' . $this->amount . '" currency="' . $this->currency . '"><department merchant_id="' . $this->merchantID . '" amount="' . $this->amount . '"/></order></merchant>';
        $merchant_sign = '<merchant_sign type="RSA">' . $this->sign64($merchant) . '</merchant_sign>';
        $xml = "<document>" . $merchant . $merchant_sign . "</document>";
        return $xml;
    }

    private function sign($str)
    {
        if ($this->privateKey) {

            openssl_sign($str, $out, $this->privateKey);

            if ($this->invert == 1) {
                $out = $this->reverse($out);
            }

            return $out;

        }
    }

    private function sign64($str)
    {
        return base64_encode($this->sign($str));
    }

    private function loadPrivateKey($filename, $password = null)
    {
        $file_path = '../keys/' . $filename;
        if (!is_file($file_path)) {

            echo "Key not found";
            return false;

        }

        $c = file_get_contents($file_path);

        if ($password) {
            $prvkey = openssl_get_privatekey($c, $password) or die(openssl_error_string());
        } else {
            $prvkey = openssl_get_privatekey($c) or die(openssl_error_string());
        }


        if (is_resource($prvkey)) {

            $this->privateKey = $prvkey;
            return $c;

        }

        return false;

    }


    public function reverse($str)
    {
        return strrev($str);
    }
}