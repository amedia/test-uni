@extends('layout.main')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Заполните форму</div>

                    <div class="panel-body">
                        <br>

                        <form class="form-horizontal" method="post" action="{{env('BANK_POST_URL')}}">
                            @csrf
                            <input type="hidden" name="Signed_Order_B64" value="{{$sign}}"/>
                            <input type="hidden" name="Language" value="eng">
                            <input type="hidden" name="FailureBackLink" value="{{route('home')}}">
                            <input type="hidden" name="BackLink" value="{{route('home')}}">
                            <input type="hidden" name="PostLink" value="{{route('postlink')}}">
                            <input type="hidden" name="FailurePostLink" value="{{route('failure_postlink')}}">
                            <div class="form-group">
                                <label for="fio" class="col-md-4 control-label">ФИО</label>
                                <div class="col-md-6">
                                    <input type="text" required
                                           class="form-control" name="fio" id="fio" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="iin" class="col-md-4 control-label">ИИН</label>
                                <div class="col-md-6">
                                    <input type="text" pattern=".{7,}" title="min. 7" required  class="form-control" name="iin" id="iin" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-md-4 control-label">Почта</label>
                                <div class="col-md-6">
                                    <input required type="email"
                                           class="form-control" name="email" id="email" placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone" class="col-md-4 control-label">Телефон</label>
                                <div class="col-md-6">
                                    <input  type="text"
                                           class="form-control" name="phone" id="phone" placeholder="">
                                </div>
                            </div>
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">Оплатить {{env('AMOUNT')}}тг.</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection