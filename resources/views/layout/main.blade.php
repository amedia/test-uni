<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('css/style.css')}}" rel="stylesheet"/>
</head>
<body cz-shortcut-listen="true">
<div id="app">
    <nav class="navbar navbar-default navbar-static-top">

        <div class="col-md-12 text-right no-padding">
            <a class=" btn btn-link" href="https://miras.app/kz/login" style="color: #fff;">KZ</a>
            <a class="active btn btn-link" href="https://miras.app/login" style="color: #fff;">RU</a>
            <a class=" btn btn-link" href="https://miras.app/en/login" style="color: #fff;">EN</a>
        </div>

        <!-- Branding Image -->
        <div class="col-md-12 text-center">
            <a class="navbar-brand " href="https://miras.app">

                <img src="https://miras.app/assets/img/logo.png">
            </a>
        </div>



    </nav>




    @yield('content')

    <!-- Footer -->
    <footer class="page-footer font-small bg-secondary  footer mt-auto py-3">
        <div class="container">
            <div class="row">

                <div class="col-sm-4">

                </div>
                <div class="col-sm-4">
                    <a href="https://miras.app/errorreport">Оповестить о проблеме</a>
                </div>
                <div class="col-sm-4">
                    <!-- Copyright -->
                    <div class="footer-copyright text-center py-3">© 2018 - 2019 Copyright:
                        <a href="https://miras.app"> Miras.app</a>
                    </div>
                    <!-- Copyright -->
                </div>



            </div>
        </div>
    </footer>
    <!-- Footer -->










</div>

<!-- Scripts -->


<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>


</body>
</html>